#Your program will accept two separate unsorted input files of numbers with equal number of lines as input.
#Each number is separated by EOL.
#Load first file into a LinkedList and then load second file into a LinkedList.
#Compute the pairwise product of the elements of these two lists.
import sys
#----------------LinkedList----------------
class node:
    def __init__(self,data=None):
        self.data = data
        self.next = None

class LinkedList:
    def __init__(self):
        self.head = node()

    def append(self,data):
        new_node = node(data)
        cur = self.head

        while cur.next != None:
            cur = cur.next

        cur.next = new_node

    def length(self):
        cur = self.head
        total = 0

        while cur.next != None:
            total += 1
            cur = cur.next

        return total

    def display(self):
        elems = []
        cur_node = self.head

        while cur_node.next != None:
            cur_node = cur_node.next
            elems.append(cur_node.data)

        print (elems)

    def get(self,index):
        if __name__ == '__main__':

            if index >= self.length():
                print("ERROR: 'get' index out of range!")
                return None

            cur_idx=0
            cur_node=self.head

            while True:
                cur_node=cur_node.next

                if cur_idx==index: return cur_node.data
                cur_idx+=1



###----------------for file1----------------###
file1 = open(sys.argv[1])
list1 = LinkedList()

#write integers into list1
lines1=file1.readlines()

for n in range(0,len(lines1)):

    x = lines1[n].split(" ")    # x is a list

    #append first digit in x
    for i in range(0,len(x)):

        y = x[i].split("\n")

        if y[0].isdigit() == 1:
            list1.append(y[0])
            break

print("First LinkedList is:")
list1.display()
print("")



###----------------for file2----------------###
file2 = open(sys.argv[2])
list2 = LinkedList()

#write integers into list1
lines2=file2.readlines()

for n in range(0,len(lines2)):

    x = lines2[n].split(" ")

    for i in range(0,len(x)):

        y = x[i].split("\n")

        if y[0].isdigit() == 1:
            list2.append(y[0])
            break

print("Second LinkedList is:")
list2.display()
print("")


###----------------control----------------
if list1.length() != list2.length():
    print("The integer numbers of the files are not equal!")
    exit()


###----------------product----------------###
products = []

for i in range(0,list1.length()):
    product = int(list1.get(i)) * int(list2.get(i))
    products.append(product)

print("Products' results:")
print(products)
print("")


#If we want to learn the product of the first elements:
print("The product of the first elements: " + str(products[0]))
