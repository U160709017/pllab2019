PROGRAMMING LANGUAGES PROJECT

Student Name: Beyza KURT
Student ID: 160709017


-SUBJECT-
Your program will accept two separate unsorted input files of numbers with equal number of lines as input.
Each number is separated by EOL.
Load first file into a LinkedList and then load second file into a LinkedList.
Compute the pairwise product of the elements of these two lists.

-FILES-
My files are in main folder. They contain unsorted integers.

-JAVA-
My Java code is in PL folder.

-PYTHON-
My Python code is in main folder.
