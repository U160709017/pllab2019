/*
 * Your program will accept two separate unsorted input files of numbers with equal number of lines as input.
 * Each number is separated by EOL.
 * Load first file into a LinkedList and then load second file into a LinkedList.
 * Compute the pairwise product of the elements of these two lists.
 * 
 */


import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Scanner;

public class PL {

	public static void main(String[] args) throws FileNotFoundException, IOException {

		File file1 = new File(args[0]);
		File file2 = new File(args[1]);
		
		Scanner s1 = new Scanner(file1);
		Scanner s2 = new Scanner(file2);

		LinkedList<Integer> list1 = new LinkedList<>();
		LinkedList<Integer> list2 = new LinkedList<>();

		// file1's line number -> l1
		BufferedReader reader1 = new BufferedReader(new FileReader("file1.txt"));
		int l1 = 0;
		while (reader1.readLine() != null)
			l1++;
		reader1.close();

		// file2's line number -> l2
		BufferedReader reader2 = new BufferedReader(new FileReader("file2.txt"));
		int l2 = 0;
		while (reader2.readLine() != null)
			l2++;
		reader2.close();

		// write file1's elements into LinkedList(list1)
		for (int i = 0; i < l1; i++) {

			if ( s1.hasNextInt() == false) {
				break;
			}
		
			if( s1.hasNextLine() == false ) {
				break;
			}
			else if( i != 0) { s1.nextLine(); }
			
			list1.add(i, s1.nextInt());
			
		}
		
		System.out.println("First LinkedList is: " + list1);
		
		// write file2's elements into LinkedList(list2)
		for (int j = 0; j < l2; j++) {

			if ( s2.hasNextInt() == false) {
				break;
			}
		
			if( s2.hasNextLine() == false ) {
				break;
			}
			else if( j != 0) { s2.nextLine(); }
			
			list2.add(j, s2.nextInt());
			
		}
		
		System.out.println("Second LinkedList is: " + list2);
		
/*		
 * 
 * This version keeps all integers
 * For example, if there are two integers in the first line,
 * it writes both of them into linked list
 * 
 * 
		for (int i = 0; i < l1; i++) {

			if (s1.hasNextInt() == false) {
				break;
			} else {
				list1.add(i, s1.nextInt());
				int ith = list1.get(i);
				System.out.println(ith);
			}
		}
	
		for (int j = 0; j < l2; j++) {

			if (s2.hasNextInt() == false) {
				break;
			} else {
				list2.add(j, s2.nextInt());
				int jth = list2.get(j);
				System.out.println(jth);
			}
		}
*/
		
		if (list1.size() != list2.size() ) {
			System.out.println("The integer numbers of the files are not equal!");
			System.exit(0);
		}
		
		
		//I want to keep product results into a list
		ArrayList<Integer> products = new ArrayList<Integer>();
		
		int product = 0;
		for( int n = 0; n < list1.size(); n++ ) {
			
			product = list1.get(n) * list2.get(n);
			products.add(n, product);
			
		}
		
		System.out.println("Products' results: " + products);
		
		//If we want to learn the product of the first elements:
		System.out.println("The product of the first elements: " + products.get(0));
		
		
		s1.close();
		s2.close();
	}

}
