use strict;
use warnings;

my $filename = $ARGV[0];

open IN, '<', $filename or die "cannot open $filename!/n";
my @lines = <IN>;
close IN;
foreach my $line (@lines) {
	chomp $line;
	if ($line =~
	/^(.*)\s+\-\s+\-\s+(\[.*\])\s+\".*\"\s+(\d{3})\s+(\d+)/) {
	my $clientip = $1;
	my $date = $2;
	my $code = $3;
	my $bytesize = $4;
	print "$clientip\t$bytesize\t$date\t$code\t";
} 
}
