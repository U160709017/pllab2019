#Regular Expression
#Meta Characters

use strict;
use warnings;

print "type q or quit to exit\n";
print "Enter your input:";
my $input = <>;
chomp $input;

my $regex = '^\s*$';
$regex = '^[A-Z]+$';
$regex = '^[A-Z]\d*$';
$regex = '^\d+\.\d+$';
$regex = '^(\d{1,3}\.){3}\d{1,3}$';
#$regex = '';

until(($input eq "quit")||($input eq "q")||($input eq "Q")) {
	if($input =~ /$regex/) { print "ACCEPTED\n"; }
	else { print "FAILED\n";}
	print "type q or quit to exit\n";
	print "Enter your input:";
	$input = <>;
	chomp $input;
}
